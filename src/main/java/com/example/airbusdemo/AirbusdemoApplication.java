package com.example.airbusdemo;

//import com.fasterxml.jackson.databind.ObjectMapper;
import com.myntra.airbus.consumer.AirbusConsumer;
import com.myntra.airbus.consumer.Consumer;
import com.myntra.airbus.entry.EventEntry;
import com.myntra.airbus.entry.EventListenerEntity;
import com.myntra.airbus.exception.ManagerException;
import com.myntra.airbus.producer.Producer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

import javax.annotation.PostConstruct;
import java.util.EventListener;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;


@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class })
public class AirbusdemoApplication {


    public static void main(String[] args) {
        SpringApplication.run(AirbusdemoApplication.class, args);
    }

    @PostConstruct
    public void consume(){
        try {
            EventEntry airbusEventOne = new EventEntry("nithin_airbustest2561", "testnithinevent");
            LocalEventConsumerOne localEventConsumerOne = new LocalEventConsumerOne();
            Map<String, Object> config = new HashMap<>();
            config.put("max.poll.interval.ms", 30000);
            EventListenerEntity eventListenerEntity = new EventListenerEntity(airbusEventOne, 1, localEventConsumerOne);
            Consumer consumer = new AirbusConsumer(eventListenerEntity, "http://platformairbus.stage.myntra.com", config,"nithin_airbustest2561");
            consumer.start();

//                            TimeUnit.SECONDS.sleep(50);

            for (Map.Entry<String, Integer> e: localEventConsumerOne.getMap().entrySet()){
                if (e.getValue()!=3){
                    throw new RuntimeException("Race");
                }
            }
        }catch (Exception e){
            throw new RuntimeException("consume failed", e);
        }
    }
}
