package com.example.airbusdemo;

//import com.fasterxml.jackson.databind.ObjectMapper;
import com.myntra.airbus.entry.EventEntry;
import com.myntra.airbus.producer.Producer;
import com.myntra.airbus.producer.impl.ProducerImpl;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.Map;

public class ProducerThread implements Runnable {
//    @Autowired
    Producer producer;
    Integer start;
    Integer count;
    public ProducerThread(Producer producer, int start, int count) {
        this.producer = producer;
        this.start=start;
        this.count=count;
    }

    @Override
    public void run() {
        try {
//            Producer producer = new ProducerImpl("http://platformairbus.stage.myntra.com", "nithin_airbustest2561");
            for(Integer i=this.start;i<this.count;i+=1) {
//                ObjectMapper mapper = new ObjectMapper();
                EventEntry e = new EventEntry();
                e.setAppName("nithin_airbustest2561");
                e.setEventName("testnithinevent");

                //Map<String, String> s = new HashMap<>();
                //s.put("testheader", "yo");
                //e.setHeaders(s);
                AirbusData airbusData = new AirbusData();
                //airbusData.setName("kadala");
                //airbusData.setData("mavu");
                // data to send
//                e.setData(mapper.writeValueAsString(airbusData));
                e.setData(i.toString());
                this.producer.send(e);
//                System.out.println("ASDASDASDASDASDASDASDASD");
//                Thread.sleep(1000L);
            }
        }catch (Exception e) {
            throw new RuntimeException("produce failed", e);
        }
    }
}
