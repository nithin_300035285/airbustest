package com.example.airbusdemo;

import lombok.Data;

@Data
public class AirbusData {
    private String name;
    private String data;
}
