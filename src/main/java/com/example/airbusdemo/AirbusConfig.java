package com.example.airbusdemo;

import com.myntra.airbus.exception.ManagerException;
import com.myntra.airbus.producer.Producer;
import com.myntra.airbus.producer.impl.ProducerImpl;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;

@Configuration
public class AirbusConfig {


//    @Bean
//    public Producer AirbusProducer() {
//        try {
//            Producer producer = new ProducerImpl("http://platformairbus.stage.myntra.com", "nithin_airbustest2561");
//            return producer;
//        }catch (Exception e){
//            throw new BeanCreationException("Producer create error",e);
//        }
//    }

//    @Bean
//    public TaskExecutor taskExecutor() {
//        return new SimpleAsyncTaskExecutor(); // Or use another one of your liking
//    }

    @Bean
    public CommandLineRunner schedulingRunner() throws ManagerException {
        TaskExecutor executor = new SimpleAsyncTaskExecutor();
        Producer producer = new ProducerImpl("http://platformairbus.stage.myntra.com", "nithin_airbustest2561");
        return new CommandLineRunner() {
            public void run(String... args) throws Exception {
                executor.execute(new ProducerThread(producer,0, 1000));
                executor.execute(new ProducerThread(producer,1000, 2000));
                executor.execute(new ProducerThread(producer,2000, 3000));
                executor.execute(new ProducerThread(producer,3000, 4000));


            }
        };
    }
//    @Bean
//    public Consumer AirbusConsumer() {
//        try {
//            Producer producer = new ProducerImpl("service_url", "appname");
//            return null;
//        }catch (Exception e){
//            throw new BeanCreationException("Consumer create error",e);
//        }
//    }
}
